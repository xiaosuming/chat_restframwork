from rest_framework import routers, permissions
from chat.views import GroupChatViewSet, MessageViewSet, MyfriendViewSet, EnclosureViewSet,  MyfriendView
from application.views import ApplicationViewSet, NoticeViewSet, TaskViewSet, UserExtensionViewSet
from restfromwork import settings
from django.contrib.staticfiles.views import serve
from django.urls import path, include, re_path
from django.conf import settings
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi



def wrap_swagger(view):
    """
    接受yasg的get_schema_view函数构造的SchemaView类视图,
    将其应用于收到的request对象
    Args:
        view: get_scheme_view函数
    Returns:
    """
    # @login_required
    def _map_format_to_schema(request, scheme=None):
        """
        接受一个请求对象和一个scheme参数,
        将请求中的format参数值映射为请求的scheme参数值
        """
        if 'format' in request.GET:
            request.GET = request.GET.copy()  # 复制请求参数
            # 获取默认文档格式schema
            format_alias = settings.REST_FRAMEWORK['URL_FORMAT_OVERRIDE']
            # 用请求参数中的format参数的值替换schema的值
            request.GET[format_alias] = request.GET['format']

        # 将更新参数的request对象和format值作为实参传递给get_schema_view函数
        return view(request, format=scheme)
    return _map_format_to_schema

# get_schema_view生成SchemaView类实例
schema_view = get_schema_view(
    openapi.Info(  # openapi.Info返回SwaggerInfo对象
        title="ITAG REST API",
        default_version='v1',
        description="REST API for Image Tag Backend (ITAG)",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

def return_static(request, path, insecure=True, **kwargs):
    return serve(request, path, insecure, **kwargs)

# 应用的url
application_routers = routers.DefaultRouter(trailing_slash=False)
application_routers.register(r'applications', ApplicationViewSet)
application_routers.register(r'notices', NoticeViewSet)
application_routers.register(r'tasks', TaskViewSet)
application_routers.register(r'userextensions', UserExtensionViewSet)

# 聊天的url
chat_routers = routers.DefaultRouter(trailing_slash=False)
chat_routers.register(r'groupchats', GroupChatViewSet)
chat_routers.register(r'messages', MessageViewSet)
chat_routers.register(r'myfriends', MyfriendViewSet)
chat_routers.register(r'enclosures', EnclosureViewSet)
chat_urls = [path('myfriendslist', MyfriendView.as_view()),
             ] + chat_routers.urls

urlpatterns = [
    path('', include(chat_routers.urls)),
    path('', include(application_routers.urls)),
    path('', include((chat_urls, 'chat.apps'), namespace='chat')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/swagger<str:scheme>',
         wrap_swagger(schema_view.without_ui(cache_timeout=0)),
         name='schema-json'),
    path('api/swagger/',
         wrap_swagger(schema_view.with_ui('swagger', cache_timeout=0)),
         name='schema-swagger-ui'),
    path('api/docs/',
         wrap_swagger(schema_view.with_ui('redoc', cache_timeout=0)),
         name='schema-redoc'),
]
