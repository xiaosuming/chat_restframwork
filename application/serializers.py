from django.db.transaction import atomic
from rest_framework import serializers
from application.models import Application, Task, Notice, UserExtension
from chat.models import Myfriend


class UserExtensionSerializer(serializers.ModelSerializer):
    remark = serializers.SerializerMethodField(help_text="自定义属性_备注")  # 重写字段
    ismyfriend = serializers.SerializerMethodField(help_text="自定义属性_好友")  # 重写字段
    def get_remark(self, instance):
        login_user = self.context['request'].user
        mf = Myfriend.objects.filter(myfriendslist_id=instance.id, owner_id=login_user.id).first()
        if mf:
            remark = mf.remark
            return remark
        else:
            return None

    def get_ismyfriend(self, instance):
        login_user = self.context['request'].user
        mf = Myfriend.objects.filter(myfriendslist_id=instance.id, owner_id=login_user.id).first()
        if mf:
            ismyfriend = 1
        else:
            ismyfriend = 0
        return ismyfriend

    username = serializers.ReadOnlyField(
        source='user.username',
        allow_null=True)  # 外键序列化的字段

    login_name = serializers.ReadOnlyField(
        source='user.extension.login_name',
        allow_null=True)  # 外键序列化的字段

    colour = serializers.ReadOnlyField(
        source='user.extension.colour',
        allow_null=True)  # 外键序列化的字段

    class Meta:
        model = UserExtension
        fields = ('id', 'login_name', 'username', 'login_name', 'remark', 'colour', 'ismyfriend')


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'

    def to_representation(self, instance):  # 展示给前端的替换和拼接
        attribute = super().to_representation(instance)
        attribute["img"] = instance.get_img_url()
        return attribute


class ApplicationSerializer(serializers.ModelSerializer):  # 自定义一些字段，直接作为序列化字段，也可以重新定义模型中自带的字段，但是尽量换个别名。
    num = serializers.SerializerMethodField(help_text="自定义属性")

    def get_num(self, instance):  # 进行自定义字段的重写
        num = Notice.objects.filter(application_id=instance.id, isreaded=0).count()
        return num  # 返回结果

    class Meta:
        model = Application
        fields = ('name', 'icon', 'num')

    def to_representation(self, instance):  # 展示给前端的替换和拼接
        attribute = super().to_representation(instance)
        attribute["icon"] = instance.get_icon_url()
        return attribute


class NoticeSerializer(serializers.ModelSerializer):
    # user = UserExtensionSerializer(many=False, read_only=True)

    application_name = serializers.ReadOnlyField(
        source='application.name',
        allow_null=True)  # 外键序列化的字段

    # username = serializers.ReadOnlyField(
    #     source='user.username',
    #     allow_null=True)  # 外键序列化的字段
    #
    # login_name = serializers.ReadOnlyField(
    #     source='user.extension.login_name',
    #     allow_null=True)  # 外键序列化的字段
    #
    # colour = serializers.ReadOnlyField(
    #     source='user.extension.colour',
    #     allow_null=True)  # 外键序列化的字段

    # task_url = serializers.CharField(
    #     source='task__task_url',
    #     allow_null=True,required=False,write_only=True)  # 外键序列化的字段,只要不是只读的，都要给他指定required字段
    #
    # task_name = serializers.CharField(
    #     source='task__task_name',
    #     allow_null=True,required=False,write_only=True)  # 外键序列化的字段，只要不是只读的，都要给他指定required字段

    task_detail = serializers.SerializerMethodField(help_text="自定义属性_任务详情")  # 重写字段

    def get_task_detail(self, instance):  # 进行自定义字段的重写
        task = instance.tasks.first()
        if task:
            task_detail = {'task_name': task.task_name, 'task_url': task.task_url},
            return task_detail  # 返回结果

    class Meta:
        model = Notice
        fields = (
        'notice_content', 'notice_type', 'sender_type', 'isreaded', 'isaccept', 'application', 'application_name',
        'user',
        'owner', 'task_detail')  # 这里序列化的字段都是参与后期增查删改的
        read_only_fields = ('username', 'login_name', 'colour', 'owner_id')  # 这里面的字段只是呈现，而不参与增查删改
        depth=1

    def create(self, validate_data):  # 对于新增的时候，需要增加第三张表，那么就需要在这里面重写
        print(validate_data)
        task_url = validate_data.pop('task__task_url')  # 这表示从上面的字段，把这些字段踢出来，用于后期的建立另外一张表
        task_name = validate_data.pop('task__task_name')  # 这表示从上面的字段，把这些字段踢出来，用于后期的建立另外一张表

        with atomic():
            notice = super().create(validate_data)  # 建立新的消息
            application_id = notice.application_id

        task = Task.objects.create(
            task_name=task_name, task_url=task_url, application_task_id=application_id,
            user_id=validate_data['user'].id, owner_id=validate_data['owner'].id)  # 同时建立消息对应的任务
        task.save()
        notice.tasks.add(task)  # 把消息和任务关联到第三张表
        return notice
