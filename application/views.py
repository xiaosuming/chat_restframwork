from django.db.models import Max
from django.utils.decorators import method_decorator
from django_filters import rest_framework as filters
from rest_framework import viewsets, status, mixins
from rest_framework.decorators import action
from rest_framework.exceptions import NotAcceptable
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework.viewsets import GenericViewSet
from application.models import Application, Notice, Task, UserExtension
from application.serializers import ApplicationSerializer, NoticeSerializer, TaskSerializer, UserExtensionSerializer


class ApplicationFilter(filters.FilterSet):  # 筛选过滤器
    """应用过滤器"""

    id = filters.CharFilter(field_name="id")  # 自己的字段
    notice = filters.CharFilter(field_name="notices__notice_content", lookup_expr="icontains")  # 外键字段

    class Meta:
        model = Application
        fields = ('id', 'notice')


class NoticeFilter(filters.FilterSet):  # 筛选过滤器
    """消息过滤器"""

    notice_content = filters.CharFilter(field_name="notice_content", lookup_expr="icontains")  # 自己的字段
    isreaded = filters.BooleanFilter(field_name="isreaded")  # 自己的字段
    application = filters.CharFilter(field_name="application")  # 自己的字段，这种也属于自己的字段

    class Meta:
        model = Notice
        fields = ('notice_content', 'isreaded', 'application')


@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['应用列表展示'], operation_summary='返回已经安装的应用系统',
    manual_parameters=[

        # openapi.Parameter('name', openapi.IN_QUERY, description='系统名称',
        #                   type=openapi.TYPE_STRING),
        # openapi.Parameter('developer', openapi.IN_QUERY, description='开发者名称',
        #                   type=openapi.TYPE_STRING),
        # openapi.Parameter('limit', openapi.IN_QUERY, description='每页返回的数量',
        #                   type=openapi.TYPE_INTEGER),
        # openapi.Parameter('offset', openapi.IN_QUERY, description='从第几条返回',
        #                   type=openapi.TYPE_INTEGER),
    ]#适用于get查询的参数
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['应用列表展示'], operation_summary='创建应用系统',
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT, description="创建应用系统",
        properties={
            'name': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                type=openapi.TYPE_INTEGER, description="")),#这个是自定义参数
            'icon': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                type=openapi.TYPE_INTEGER, description=""))}, )))#这个是自定义参数


# @method_decorator(name='retrieve', decorator=swagger_auto_schema(
#     tags=['应用列表展示'], operation_summary='展示单个应用系统详情'
# ))
# @method_decorator(name='update', decorator=swagger_auto_schema(
#     tags=['应用商店-应用系统'], operation_summary='更新应用系统'
# ))
# @method_decorator(name='partial_update', decorator=swagger_auto_schema(
#     tags=['应用商店-应用系统'], operation_summary='更新应用系统指定的属性'
# ))
# @method_decorator(name='destroy', decorator=swagger_auto_schema(
#     tags=['应用商店-应用系统'], operation_summary='删除应用系统'
# ))


class ApplicationViewSet(mixins.ListModelMixin, mixins.CreateModelMixin, GenericViewSet):
    """
    应用列表的展示和未读消息的统计
    """

    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer

    # 重写查询
    def get_queryset(self):
        log_user = self.request.user
        querysets = Application.objects.annotate(latest_notice_time=Max('notices__send_time')).order_by(
            '-latest_notice_time')
        queryset = querysets.filter(owner_id=log_user.id)
        return queryset#直接返回查询结果

    # 重写新建
    def create(self, request, *args, **kwargs):
        login_user = request.user

        # 添加评论
        if request.method == 'POST':
            application_data = {
                "owner_id": login_user.id,
                "name": request.data.get("name"),
                "icon": request.data.get("icon"),
            }#这是简单的集成写法
            app = Application.objects.filter(name=application_data['name']).first()
            if app:
                raise NotAcceptable(detail="应用已经存在")
            else:
                db_comment = Application.objects.create(**application_data)
                serializer = ApplicationSerializer(db_comment, context={'request': request})
                return Response(data=serializer.data, status=status.HTTP_200_OK)#返回新建的结果

    def perform_create(self, serializer):#自己不需要输入的，通过这个自动输入
        login_user = self.request.user
        serializer.save(user=login_user)


@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['通知展示'], operation_summary='返回每个应用对应的通知',
    manual_parameters=[
        openapi.Parameter('notice_content', openapi.IN_QUERY, description='通知内容',
                          type=openapi.TYPE_STRING),
        openapi.Parameter('isreaded', openapi.IN_QUERY, description='是否已读',
                          type=openapi.TYPE_STRING),
        openapi.Parameter('application', openapi.IN_QUERY, description='应用',
                          type=openapi.TYPE_INTEGER),
    ]
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['通知展示'], operation_summary='接收新的应用通知', ))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['通知展示'], operation_summary='备用接口'
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['通知展示'], operation_summary='某一条消息的修改'
))
class NoticeViewSet(mixins.ListModelMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    """
    消息新建，筛选和查看，修改
    """
    queryset = Notice.objects.all()
    serializer_class = NoticeSerializer
    filterset_class = NoticeFilter
    search_field = ('notice_content', 'isreaded', 'application')
    pagination_class = None

    def perform_create(self, serializer):
        login_user = self.request.user
        serializer.save(owner=login_user)

    # def partial_update(self, request, *args, **kwargs):
    #     """更新通知参数"""
    #
    #     # isreaded = request.data.get("isreaded", None)
    #     isaccept = request.data.get("isaccept", None)
    #
    #     if isaccept ==1:
    #         instance = self.get_object()
    #         instance.update(isaccept=1)
    #         instance.update(isreaded=1)
    #         return Response(status=status.HTTP_200_OK)
    #     else:
    #         instance = self.get_object()
    #         instance.update(isaccept=0)
    #         instance.update(isreaded=1)
    #         return Response(status=status.HTTP_200_OK)



    @swagger_auto_schema(method='patch', operation_summary='把某一个应用的消息全部变为已读', tags=['通知展示'],
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="创建应用系统",
                             properties={
                                 'application_id': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                     type=openapi.TYPE_INTEGER, description="")),
                                 'isreaded': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                     type=openapi.TYPE_INTEGER, description="")), }, ))
    @action(methods=['patch'], detail=False)
    def updations(self, request, *args, **kwargs):
        """把某一个应用的消息全部变为已读"""
        application_id = request.data.get("application_id", None)
        isreaded = request.data.get("isreaded", None)
        if isreaded is not None and application_id is not None:
            Notice.objects.filter(application_id=application_id).update(isreaded=isreaded)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

    @swagger_auto_schema(method='delete', operation_summary='删除全部的通知', tags=['通知展示'])
    @action(methods=['delete'], detail=False)
    def deletions(self, request, *args, **kwargs):
        """删除全部的通知"""
        notice_objs = Notice.objects.filter(owner=request.user)
        notice_objs.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['任务'], operation_summary='返回每个消息对应的任务',
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['任务'], operation_summary='接收新的任务'
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    tags=['任务'], operation_summary='展示单个任务'
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['任务'], operation_summary='备用接口'
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['任务'], operation_summary='修改某一条任务'
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['任务'], operation_summary='删除任务'
))
class TaskViewSet(viewsets.ModelViewSet):
    """
   任务的增查删改
    """
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def perform_create(self, serializer):
        login_user = self.request.user
        serializer.save(owner=login_user)



class UserExtensionFilter(filters.FilterSet):
    """应用过滤器"""
    username = filters.CharFilter(field_name="user__username", lookup_expr="icontains")

    class Meta:
        model = UserExtension
        fields = ("username",)

@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['用户列表'], operation_summary='返回详细的用户列表',
))
class UserExtensionViewSet(mixins.ListModelMixin, GenericViewSet):
    """
    用户扩展的增查删改
    """
    queryset = UserExtension.objects.all()
    serializer_class = UserExtensionSerializer
    filterset_class = UserExtensionFilter
    search_field = ('username',)
    pagination_class = None
