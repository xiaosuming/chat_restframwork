from collections import OrderedDict
from django.contrib.auth.models import User
from django.db.models import Max, Q
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from chat.models import GroupChat, Message, Myfriend, MessagePart, GroupChatPart, Enclosure
from chat.serializers import GroupChatSerializer, MessageSerializer, MyfriendSerializer, EnclosureSerializer
from django.db.models import Max
from django.utils.decorators import method_decorator
from django_filters import rest_framework as filters
from rest_framework import viewsets, status, mixins
from rest_framework.decorators import action
from rest_framework.exceptions import NotAcceptable
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from application.models import Notice


class GroupChatFilter(filters.FilterSet):
    """应用过滤器"""
    username = filters.CharFilter(field_name="groupchat_members__username", lookup_expr="icontains")
    class Meta:
        model = GroupChat
        fields = ("username",)

@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['群列表展示'], operation_summary='返回群列表',
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['群列表展示'], operation_summary='创建群聊',
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['群列表展示'], operation_summary='转让管理员 和 修改群名 和 删除聊天列表中的某个聊天',
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['群列表展示'], operation_summary='备用接口',
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['群列表展示'], operation_summary='解散群组',
))
class GroupChatViewSet(mixins.ListModelMixin, mixins.CreateModelMixin,mixins.UpdateModelMixin,mixins.DestroyModelMixin, GenericViewSet):
    """
    群组的增查删改
    """
    queryset = GroupChat.objects.all()
    serializer_class = GroupChatSerializer
    filterset_class = GroupChatFilter
    search_field = ("username",)
    pagination_class = None

    # 重写新建
    def create(self, request, *args, **kwargs):
        global i, serializer
        login_user = request.user

        # 添加评论
        privatechat = []
        if request.method == 'POST':
            groupchat_name = request.data.get("groupchat_name")
            is_groupchat = request.data.get("is_groupchat")
            userlist = request.data.get("userlist")
            if is_groupchat == 0:
                for i in userlist:
                    privatechat = GroupChat.objects.filter(
                        Q(groupchat_members__id=i) & Q(
                            is_groupchat=False)).filter(
                        groupchat_members__id__icontains=login_user.id).first()
                    if privatechat:
                        raise NotAcceptable(detail="群组已经存在或群名已经存在")
                    else:
                        username = User.objects.filter(id=userlist[0]).first().username
                        db_comment = GroupChat.objects.create(groupchat_name=username, is_groupchat=0)
                        serializer = GroupChatSerializer(db_comment, context={'request': request})
                        db_comment.groupchat_members.add(login_user)
                        for u in userlist:
                            db_comment.groupchat_members.add(u)
                        return Response(data=serializer.data, status=status.HTTP_200_OK)
            elif is_groupchat == 1:
                groupchat = GroupChat.objects.filter(groupchat_name=groupchat_name).first()
                if groupchat:
                    raise NotAcceptable(detail="群组已经存在或群名已经存在")
                else:
                    db_comment = GroupChat.objects.create(groupchat_name=groupchat_name, is_groupchat=1,
                                                          groupchat_creater_id=login_user.id)
                    serializer = GroupChatSerializer(db_comment, context={'request': request})
                    db_comment.groupchat_members.add(login_user)
                    for u in userlist:
                        db_comment.groupchat_members.add(u)
                    return Response(data=serializer.data, status=status.HTTP_200_OK)
            else:
                raise NotAcceptable(detail="参数缺失")

    # 重写查询
    def get_queryset(self):
        queryset=[]
        login_user = self.request.user
        if self.request.method == 'get':
            groupchats = GroupChat.objects.annotate(latest_msg_time=Max('messagegroupchat__send_time'))  # 获取群组中最早的消息
            groupchats = groupchats.order_by('-latest_msg_time')  # 根据每个群中最早的消息进行排序
            groupchats= groupchats.filter(groupchat_members__id__contains=login_user.id)
            groupchatparts = login_user.groupchatpartuser.all()  # 删除的聊天，
            if groupchatparts:
                a = []
                for gc in groupchatparts:
                    a.append(gc.groupchat_id)
                queryset = groupchats.exclude(id__in=a)  # 不显示删除的聊天，只显示剩余的聊天
                print(queryset)
            return queryset
        else:
            groupchats = GroupChat.objects.annotate(latest_msg_time=Max('messagegroupchat__send_time'))  # 获取群组中最早的消息
            groupchats = groupchats.order_by('-latest_msg_time')  # 根据每个群中最早的消息进行排序
            queryset= groupchats.filter(groupchat_members__id__contains=login_user.id)
            return queryset


    def perform_create(self, serializer):
        login_user = self.request.user
        serializer.save(groupchat_creater=login_user.id)

    @swagger_auto_schema(method='post', operation_summary='添加群成员', tags=['群列表展示'],
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="添加群成员",
                             properties={'user_id': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                 type=openapi.TYPE_INTEGER, description="")), }))
    @swagger_auto_schema(method='delete', operation_summary='删除群成员', tags=['群列表展示'],
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="删除群成员",
                             properties={'user_id': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                 type=openapi.TYPE_INTEGER, description="")), }))
    @action(methods=['post', 'delete'], detail=True)
    def members(self, request, pk, **kwargs):
        """ 把某一个群成员的增加和删除 """
        groupchat = self.get_object()  # 表示某一个群

        # 添加群成员
        if request.method == 'POST':
            user_id = request.data.get("user_id", None)
            if user_id is not None:
                groupchat.groupchat_members.add(user_id)
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

        # 删除群成员
        elif request.method == 'DELETE':
            user_id = request.data.get("user_id", None)
            if user_id is not None:
                groupchat.groupchat_members.remove(user_id) # 删除选中的人员
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

    @swagger_auto_schema(method='patch', operation_summary='某一个群聊消息变为已读', tags=['群列表展示'],
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="某一个群聊消息变为已读",
                             properties={'isreaded': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                 type=openapi.TYPE_INTEGER, description="")), }))

    @action(methods=['patch'], detail=True)
    def messages(self, request, pk, **kwargs):
        if request.method == 'PATCH':
            gc=self.get_object()
            isreaded = request.data.get("isreaded", None)
            gc.messagegroupchat.all().update(isreaded=isreaded)
            return Response(status=status.HTTP_200_OK)


class MessageFilter(filters.FilterSet):
    """应用过滤器"""

    text_content = filters.CharFilter(field_name="text_content", lookup_expr="icontains")

    class Meta:
        model = Message
        fields = ("text_content",)

@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['聊天消息'], operation_summary='消息展示',
    manual_parameters=[
        openapi.Parameter('username', openapi.IN_QUERY, description='群成员名称',
                          type=openapi.TYPE_STRING),
        openapi.Parameter('groupchat', openapi.IN_QUERY, description='所属群组id',
                          type=openapi.TYPE_STRING),
    ]
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['聊天消息'], operation_summary='消息创建',
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['聊天消息'], operation_summary='消息变为已读',
))

class MessageViewSet(mixins.CreateModelMixin, mixins.ListModelMixin,mixins.UpdateModelMixin, GenericViewSet):
    """
    """
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    filterset_class = MessageFilter
    search_field = ("groupchat", "text_content")
    pagination_class = None

    # 重写查询
    def get_queryset(self):
        login_user = self.request.user
        groupchat=self.request.query_params.get("groupchat")
        mp=login_user.messagepartuser.filter(groupchat_id=groupchat).first()
        if mp:
            lastmessage_id=login_user.messagepartuser.filter(groupchat_id=groupchat).last().lastmessage_id
            queryset=Message.objects.filter(id__gt=lastmessage_id,groupchat_id=groupchat)
            return queryset


    @swagger_auto_schema(method='delete', operation_summary='清空聊天记录', tags=['聊天消息'],
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="创建应用系统",
                             properties={
                                 'groupchat_id': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                     type=openapi.TYPE_INTEGER, description="")),
                                 'lastmessage_id': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                     type=openapi.TYPE_INTEGER, description="")),
                                 'user_id': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                     type=openapi.TYPE_INTEGER, description=""))
                             },))
    @action(methods=['delete'], detail=False)
    def deletions(self, request, *args, **kwargs):
        """清空聊天记录"""
        groupchat_id = request.data.get("groupchat_id")
        lastmessage_id = request.data.get("lastmessage_id")
        user_id = request.data.get("user_id")
        MessagePart.objects.create(groupchat_id=groupchat_id, lastmessage_id=lastmessage_id, user_id=user_id)
        return Response(status=status.HTTP_204_NO_CONTENT)


    @swagger_auto_schema(method='get', operation_summary='所有未读消息的统计', tags=['聊天消息'], )
    @action(methods=['get'], detail=False)
    def countions(self, request, *args, **kwargs):
        """所有未读消息的统计"""
        login_user = self.request.user
        num_1 = Notice.objects.filter(Q(isreaded=0) & Q(owner_id=login_user.id)).count()  # 统计未读通知的数量
        num_2 = Message.objects.filter(Q(isreaded=0) & Q(user_id=login_user.id)).count()  # 统计未读消息的数量
        num = num_1 + num_2
        return Response(data=num, status=status.HTTP_200_OK)

@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['好友列表'], operation_summary='好友列表展示',
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['好友列表'], operation_summary='好友申请发送',
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['好友列表'], operation_summary='修改备注',
))

class MyfriendViewSet(mixins.CreateModelMixin, mixins.ListModelMixin, mixins.UpdateModelMixin,GenericViewSet):
    """
    """
    queryset = Myfriend.objects.all()
    serializer_class = MyfriendSerializer

    # 重写查询
    def get_queryset(self):
        login_user = self.request.user
        queryset = Myfriend.objects.filter(owner_id=login_user)
        return queryset

    # 重写新建
    def create(self, request, *args, **kwargs):
        myfriendslist = request.data.get("myfriendslist")
        login_user = request.user
        if myfriendslist:  # 用户如果存在，则进行添加为好友！
            oldfriend = Myfriend.objects.filter(Q(myfriendslist__id=myfriendslist) & Q(owner_id=login_user.id))  # 验证用户是否在好友列表
            if oldfriend:  # 如果在好友列表，则提示用户已经存在！
                raise NotAcceptable(detail={"detail": "好友已经存在"})
            else:  # 如果不在好友列表。则申请添加用户为好友！
                oldnotice = Notice.objects.filter(Q(user_id=login_user.id) & Q(owner_id=myfriendslist)).last()
                if oldnotice and oldnotice.notice_type == "apply" and oldnotice.isaccept is None:
                    raise NotAcceptable(detail={"detail": "好友申请已存在"})
                elif oldnotice and oldnotice.notice_type == "apply" and oldnotice.isaccept == 0:
                    raise NotAcceptable(detail={"detail": "好友申请已存在"})
                else:
                    Notice.objects.create(
                        notice_content="申请加您为好友",
                        user_id=login_user.id,
                        owner_id=myfriendslist,
                        notice_type="apply",
                        sender_type="user",
                        isaccept=None,
                        isreaded=0,
                    )
        return Response(data={"detail": "好友申请发送成功"}, status=status.HTTP_200_OK)

    @swagger_auto_schema(method='patch', operation_summary='好友申请接受和拒绝', tags=['好友列表'],)
    @action(methods=['patch'], detail=False)
    def updations(self, request, *args, **kwargs):
        """好友申请接受和拒绝"""
        login_user = request.user
        myfriendslist = request.data.get("myfriendslist")
        is_accept = request.data.get("is_accept")
        # 校验用户和参数
        if not myfriendslist or not is_accept:
            raise NotAcceptable(detail={"detail": "参数缺失"})
        if is_accept == 1 and myfriendslist:
            oldfriend = Myfriend.objects.filter(Q(myfriendslist__id=myfriendslist) & Q(owner_id=login_user.id))  # 验证用户是否在好友列表
            if oldfriend:  # 如果在好友列表，则提示用户已经存在！
                raise NotAcceptable(detail={"detail": "好友已经存在"})
            else:
                Notice.objects.filter(Q(owner_id=login_user.id) & Q(user_id=myfriendslist) & Q(notice_type="apply")).update(
                    isaccept=is_accept)
                myfriend = Myfriend.objects.create(myfriendslist_id=myfriendslist, owner_id=login_user.id)
                myfriend.save()
                myfriend1 = Myfriend.objects.create(myfriendslist_id=login_user.id, owner_id=myfriendslist)
                myfriend1.save()
                newuser = User.objects.filter(id=myfriendslist).first()
                privatechat = GroupChat.objects.create(groupchat_name=newuser.first_name,
                                                       is_groupchat=False)  # 那么建立私聊
                message = Message.objects.create(user_id=login_user.id,
                                                 text_content='聊天已经建立，开始你们的聊天吧',
                                                 message_type='message_normal',
                                                 groupchat_id=privatechat.id)  # 那么建立私聊的第一个聊天
                MessagePart.objects.create(groupchat_id=privatechat.id, user_id=login_user.id,
                                           lastmessage_id=message.id)
                MessagePart.objects.create(groupchat_id=privatechat.id, user_id=myfriendslist, lastmessage_id=message.id)
                privatechat.groupchat_members.add(myfriendslist)  # 把用户加入群聊的人员中。
                privatechat.groupchat_members.add(login_user)  # 把管理员加入群聊的人员中。
                privatechat.save()
            return Response(data={"detail": "好友申请已通过/拒绝"}, status=status.HTTP_200_OK)




class MyfriendView(APIView):
    serializer_class = MyfriendSerializer
    @swagger_auto_schema(
        tags=['好友列表'], operation_summary='好友名字的筛选',responses={200: "成功"},
        manual_parameters=[
            openapi.Parameter('user_name', openapi.IN_QUERY, description='名称',
                              type=openapi.TYPE_STRING)],)

    def get(self, request):
        # 获取参数
        global messages, gs
        log_user = request.user
        user_name = request.GET.get('user_name')  # 获取发信人id参数

        if user_name:  # 如果关键字存在
            myfriends = Myfriend.objects.filter((Q(myfriendslist__first_name__icontains=user_name) | Q(
                myfriendslist__username__icontains=user_name) | Q(
                remark__contains=user_name)) & Q(owner_id=log_user.id))  # 则以关键字模糊筛选我的好友

            # serializer = MyfriendSerializerSerializer(cards, many=True)
            # return Response(serializer.data)



            # # 返回结果，展示出我的好友列表
            # myfriend_data = []
            # for m in myfriends:
            #     groupchat = GroupChat.objects.filter(
            #         Q(groupchat_members__id__contains=m.myfriendslist.id) & Q(is_groupchat=0)).filter(
            #         groupchat_members__id__contains=log_user.id).first()
            #     myfriendsdict = {"user_id": m.myfriendslist.id,
            #                      'user_name': m.myfriendslist.first_name,
            #                      "login_name": m.myfriendslist.username,
            #                      "remark": m.remark,
            #                      'colour': m.myfriendslist.extension.colour,
            #                      "ismyfriend": 1}
            #     if groupchat:
            #         groupchat_id = groupchat.id
            #         myfriendsdict['groupchat_id'] = groupchat_id
            #     else:
            #         myfriendsdict['groupchat_id'] = None
            #     myfriend_data.append(myfriendsdict)




            groupchats = GroupChat.objects.filter(
                Q(groupchat_members__id__contains=log_user.id) & Q(is_groupchat=1)).filter(
                Q(groupchat_members__username__icontains=user_name) | Q(
                    groupchat_members__first_name__icontains=user_name) | Q(
                    groupchat_members__myfriendslist__remark__icontains=user_name))  # 则以关键字模糊筛选群成员

            # 返回结果，展示群成员列表
            groupmembers_data = []
            for g in groupchats:
                members = g.groupchat_members.filter(Q(first_name__icontains=user_name) | Q(
                    username__icontains=user_name) | Q(
                    myfriendslist__remark__icontains=user_name))  # 反向查询每个群中和关键字相关的群成员
                num = members.count()
                allmembers = []

                for member in members:
                    mf = Myfriend.objects.filter(
                        myfriendslist__id=member.id, owner_id=log_user.id).first()
                    membersdict = {"user_id": member.id,
                                   "user_name": member.first_name,
                                   "login_name": member.username,
                                   "colour": member.extension.colour, }
                    if mf:
                        remark = mf.remark
                        membersdict['remark'] = remark
                        membersdict['ismyfriend'] = 1
                    else:
                        membersdict['ismyfriend'] = 0
                    allmembers.append(membersdict)

                groupmembers_data.append({
                    "groupchat_id": g.id,
                    'groupchat_name': g.groupchat_name,
                    "groupchat_member": allmembers,
                    'groupmembers_num': num,
                })

            messages_data = []
            allgroupchats = GroupChat.objects.filter(groupchat_members__id__icontains=log_user.id)

            # 找到和我相关的群组
            for gs in allgroupchats:  # 遍历群组，并找到每个群组中，和关键字相关的消息
                messages = Message.objects.filter(
                    (Q(user__first_name__icontains=user_name) | Q(
                        user__username__icontains=user_name) | Q(
                        user__myfriendslist__remark__icontains=user_name)) & Q(
                        groupchat_id=gs.id))

                messages_num = messages.count()  # 统计每个聊天中相关详细的数量
                messagesdict = {'groupchat_id': gs.id,
                                'groupchat_name': gs.groupchat_name,
                                'messages_num': messages_num,
                                "is_groupchat": gs.is_groupchat}
                if messages and gs.is_groupchat == 0:
                    messages_data.append(messagesdict)
                elif messages and gs.is_groupchat == 1:
                    messages_data.append(messagesdict)

            # 为列表中的字典去重
            b = OrderedDict()
            for item in messages_data:
                b.setdefault(item['groupchat_id'], {**item, })
            messages_data = list(b.values())

            b = OrderedDict()
            for item in groupmembers_data:
                b.setdefault(item['groupchat_id'], {**item, })
            groupmembers_data = list(b.values())

            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "myfriend_data": myfriend_data,
                "groupmembers_data": groupmembers_data,
                "messages_data": messages_data,
            }
            return Response(data=res_data, status=status.HTTP_200_OK)
        else:
            return Response(data={"detail": "输入内容为空，请输入要查询的内容"}, status=status.HTTP_200_OK)


@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['附件'], operation_summary='返回每个聊天对应的附件',
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['附件'], operation_summary='接收新的附件'
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    tags=['附件'], operation_summary='展示单个附件'
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['附件'], operation_summary='备用接口'
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['附件'], operation_summary='修改某一条附件'
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['附件'], operation_summary='删除附件'
))
class EnclosureViewSet(viewsets.ModelViewSet):
    """
    """
    queryset = Enclosure.objects.all()
    serializer_class = EnclosureSerializer
